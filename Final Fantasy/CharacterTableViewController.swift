//
//  CharacterTableViewController.swift
//  Final Fantasy
//
//  Created by Bruno Silva on 16/12/20.
//

import UIKit

class CharacterTableViewController: UITableViewController {
    
    //MARK: - Variables
    var characters: [String] = ["Noctis", "Prompto", "Regis Luci", "Ignis", "Gladious"]
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! InfoTableViewCell
        let character = characters[indexPath.row]
        cell.prepareCell(character)
        return cell
    }
}
